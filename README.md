# clara

Pre-alpha implementation of a Rete-based production engine in Clojure.

## Usage

None yet; see the test code for some examples.

## License

Copyright © 2013 Ryan Brush

Distributed under the Eclipse Public License, the same as Clojure.
